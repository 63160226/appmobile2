import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
  class HelloFlutterApp extends StatefulWidget {
    @override
    _HelloFlutterAppState createState() => _HelloFlutterAppState();
  }

  String englishGreeting = "Hello Flutter";
  String spanishGreeting = "Halo  Flutter";
  String thaiGreeting = "สวัสดี ฟลัตเตอร์";
  String vietnamGreeting = "Xin chào rung";
  class _HelloFlutterAppState extends State<HelloFlutterApp> {
    String displayText = englishGreeting;

    @override
    Widget build(BuildContext context) {
      return MaterialApp(debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(onPressed: () {setState(() {
                displayText = displayText == englishGreeting?vietnamGreeting:englishGreeting;

              });}, icon: Icon(Icons.translate)),

              IconButton(onPressed: () {setState(() {
                displayText = displayText == englishGreeting?thaiGreeting:englishGreeting;

              });}, icon: Icon(Icons.abc)),

              IconButton(onPressed: () {setState(() {
                displayText = displayText == englishGreeting?spanishGreeting:englishGreeting;

              });}, icon: Icon(Icons.refresh))
            ],),
          body: Center(
            child: Text(displayText, style: TextStyle(fontSize: 24),),),

        ),
      );
    }
  }

// //SafeArea
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(debugShowCheckedModeBanner: false,
//       home: Scaffold (
//         appBar: AppBar(title: Text("Hello Flutter"),leading: Icon(Icons.home),actions: <Widget>[
//           IconButton(onPressed:(){}, icon: Icon(Icons.refresh))
//         ],),
//         body: Center(child: Text("Hello Flutter",style: TextStyle(fontSize: 24),),),
//
//       ),
//     );
//   }
// }
